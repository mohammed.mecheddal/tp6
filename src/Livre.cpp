#include "Livre.hpp"
#include<string>

    Livre::Livre(){}
    Livre::Livre(const std::string & titre, const std::string & auteur, int annee):
        _titre(titre),
        _auteur(auteur),
        _annee(annee)
    {}

    const std::string & Livre::getTitre() const{
        return _titre;
    }

    const std::string & Livre::getAuteur() const{
        return _auteur;
    }

     int Livre::getAnnee() const{
        return _annee;
    }

