#ifndef LIVRE_HPP
#define LIVRE_HPP

#include<string>

class Livre
{
    public:
        Livre();
        Livre(const std::string & titre, const std::string & auteur, int annee);

        const std::string & getTitre() const;
        const std::string & getAuteur() const;
        int getAnnee() const;

    private:
        std::string _titre ;
        std::string _auteur ;
        int _annee;
};

#endif // LIVRE_HPP
